include <nutsnbolts/cyl_head_bolt.scad>
e=0.01;
/**
* A clasp for necklaces using screws and mounts 
*
*/

2e=2*e;
$fn=64;

translate([20,0,2.5]) cap();
screw_holder();
translate([-20,0,0]) nut_holder();

/**
* The part of the clamp that holds the screw in. It needs 
* a cap, see below
*
*/
module screw_holder(){
    difference(){
        translate([0,0,6])  cube([10,10,12], true);
        translate([0,0,6]) hole_through("M4");
        // translate([0,0,6]) screw("M4x16");
        translate([0,0,6-e]) cylinder(d=8, h=30);
    }
    //translate([0,0,6]) screw("M4x16");
}

/**
* The cap of the screw holder where the filament is mounted.
* I think that melting the filament into a blob with a lighter
* should make it thick enough to not be able to pass through the 
* hole.
*
*/
module cap(){
    rotate([180,0,-0]) difference(){
        union(){
            cube([10,10,5], true);
            translate([0,0,-5])  cylinder(d=8, h=3);
        }
        
        translate([0,0,-10])  cylinder(d=2, h=30);
    }
}

/**
* The part of the clamp that holds the nut and has a hole
* for the filament. The nut should be glued or friction-mounted
* in
*
*/
module nut_holder(){
    rotate([180,0,0]) difference(){
        translate([0,0,-4])  cube([10,10,8], true);
        translate([0,0,-4-e]) nutcatch_parallel("M4", clk=0.1);
        translate([0,0,-10]) cylinder(d=2, h=30);
    }
}