# Various 3D printed pieces of jewellery

## Coupling

The coupling can be assembled using one M4x16 screw and one M4 nut like this

![Coupling assembly photo](coupling-assembly.jpg)